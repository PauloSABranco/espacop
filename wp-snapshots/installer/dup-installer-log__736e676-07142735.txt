********************************************************************************
* DUPLICATOR-LITE: Install-Log
* STEP-1 START @ 02:30:30
* VERSION: 1.3.10
* NOTICE: Do NOT post to public sites or forums!!
********************************************************************************
PHP:		7.3.3 | SAPI: apache2handler
PHP MEMORY:	4294967296 | SUHOSIN: disabled
SERVER:		Apache/2.4.38 (Win64) OpenSSL/1.1.1b PHP/7.3.3
DOC ROOT:	C:/xampp/htdocs/espacop
DOC ROOT 755:	true
LOG FILE 644:	true
REQUEST URL:	http://localhost/espacop/dup-installer/main.installer.php
SAFE MODE :	0
CONFIG MODE :	NEW
--------------------------------------
PRE-EXTRACT-CHECKS
--------------------------------------
- PASS: Apache '.htaccess' not found - no backup needed.
- PASS: Microsoft IIS 'web.config' not found - no backup needed.
- PASS: WordFence '.user.ini' not found - no backup needed.
--------------------------------------
ARCHIVE SETUP
--------------------------------------
NAME:	20190418_espacop_736e676197912b6d1459_20191007142735_archive.zip
SIZE:	180.94MB

>>> Starting ZipArchive Unzip
ZipArchive Object
(
    [status] => 0
    [statusSys] => 0
    [numFiles] => 7032
    [filename] => C:\xampp\htdocs\espacop\20190418_espacop_736e676197912b6d1459_20191007142735_archive.zip
    [comment] => 
)
File timestamp set to Current: 2019-10-07 14:31:32
<<< ZipArchive Unzip Complete: true
--------------------------------------
POST-EXTACT-CHECKS
--------------------------------------

PERMISSION UPDATES: None Applied

STEP-1 COMPLETE @ 02:31:32 - RUNTIME: 61.2643 sec.



********************************************************************************
* DUPLICATOR-LITE INSTALL-LOG
* STEP-2 START @ 02:32:11
* NOTICE: Do NOT post to public sites or forums!!
********************************************************************************
--------------------------------------
DATABASE-ENVIRONMENT
--------------------------------------
MYSQL VERSION:	This Server: 10.1.38 -- Build Server: 5.7.27
FILE SIZE:	dup-database__736e676-07142735.sql (4.55MB)
TIMEOUT:	5000
MAXPACK:	1048576
SQLMODE:	NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION
NEW SQL FILE:	[C:/xampp/htdocs/espacop/dup-installer/dup-installer-data__736e676-07142735.sql]
COLLATE FB:	Off
*** PHP Warning Message: count(): Parameter must be an array or an object that implements Countable (Code: 2, line 373 in C:\xampp\htdocs\espacop\dup-installer\ctrls\ctrl.s2.dbinstall.php)
--------------------------------------
DATABASE RESULTS
--------------------------------------
DB VIEWS:	enabled
DB PROCEDURES:	enabled
ERRORS FOUND:	0
DROPPED TABLES:	0
RENAMED TABLES:	0
QUERIES RAN:	164

wp_backup_and_move: (2)
wp_cntctfrm_field: (13)
wp_commentmeta: (0)
wp_comments: (1)
wp_duplicator_packages: (0)
wp_links: (7)
wp_option_tree: (169)
wp_options: (271)
wp_postmeta: (811)
wp_posts: (924)
wp_term_relationships: (20)
wp_term_taxonomy: (15)
wp_termmeta: (0)
wp_terms: (15)
wp_thumbnail_slider: (0)
wp_usermeta: (52)
wp_users: (3)
Removed '28' cache/transient rows

INSERT DATA RUNTIME: 2.7490 sec.
STEP-2 COMPLETE @ 02:32:14 - RUNTIME: 2.7636 sec.



********************************************************************************
DUPLICATOR-LITE INSTALL-LOG
STEP-3 START @ 02:32:34
NOTICE: Do NOT post to public sites or forums
********************************************************************************
CHARSET SERVER:	latin1
CHARSET CLIENT:	utf8

--------------------------------------
SERIALIZER ENGINE
[*] scan every column
[~] scan only text columns
[^] no searchable columns
--------------------------------------
wp_backup_and_move~ (2)
wp_cntctfrm_field~ (13)
wp_commentmeta^ (0)
wp_comments~ (1)
wp_duplicator_packages^ (0)
wp_links~ (7)
wp_option_tree~ (169)
wp_options~ (271)
wp_postmeta~ (811)
wp_posts~ (924)
wp_term_relationships~ (20)
wp_term_taxonomy~ (15)
wp_termmeta^ (0)
wp_terms~ (15)
wp_thumbnail_slider^ (0)
wp_usermeta~ (52)
wp_users~ (3)
--------------------------------------
Search1:	'/home/espacop/public_html' 
Change1:	'C:/xampp/htdocs/espacop' 
Search2:	'\/home\/espacop\/public_html' 
Change2:	'C:\/xampp\/htdocs\/espacop' 
Search3:	'%2Fhome%2Fespacop%2Fpublic_html%2F' 
Change3:	'C%3A%2Fxampp%2Fhtdocs%2Fespacop%2F' 
Search4:	'\home\espacop\public_html' 
Change4:	'C:/xampp/htdocs/espacop' 
Search5:	'\\home\\espacop\\public_html' 
Change5:	'C:\/xampp\/htdocs\/espacop' 
Search6:	'%5Chome%5Cespacop%5Cpublic_html' 
Change6:	'C%3A%2Fxampp%2Fhtdocs%2Fespacop' 
Search7:	'//espaco-p.pt' 
Change7:	'//localhost/espacop' 
Search8:	'\/\/espaco-p.pt' 
Change8:	'\/\/localhost\/espacop' 
Search9:	'%2F%2Fespaco-p.pt' 
Change9:	'%2F%2Flocalhost%2Fespacop' 
Search10:	'https://localhost' 
Change10:	'http://localhost' 
Search11:	'https:\/\/localhost' 
Change11:	'http:\/\/localhost' 
Search12:	'https%3A%2F%2Flocalhost' 
Change12:	'http%3A%2F%2Flocalhost' 
SCANNED:	Tables:17 	|	 Rows:2303 	|	 Cells:27351 
UPDATED:	Tables:1 	|	 Rows:1003 	|	 Cells:1396 
ERRORS:		0 
RUNTIME:	0.529500 sec

====================================
CONFIGURATION FILE UPDATES:
====================================

UPDATED WP-CONFIG ARK FILE:
 - 'C:/xampp/htdocs/espacop/wp-config.php'
- PASS: Successfully created a new .htaccess file.
- PASS: Existing Apache 'htaccess.orig' was removed

====================================
NOTICES
====================================

No General Notices Found


STEP-3 COMPLETE @ 02:32:35 - RUNTIME: 0.5824 sec. 


